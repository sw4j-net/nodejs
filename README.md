# Introduction

This project creates a docker image with [node.js](https://nodejs.org/) installed.

The image can be used to build software using node.js.

This repository is mirrored to https://gitlab.com/sw4j-net/nodejs

ARG CI_REGISTRY
FROM debian:stretch-backports

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install curl gnupg2 && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get -y install nodejs
